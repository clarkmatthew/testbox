#!/usr/bin/python

from setuptools import setup, find_packages

__version__ = '0.1'

setup(name="tpy",
      version=__version__,
      description="General Test, and Network Utilities",
      long_description="General Test, and Network Utilities",
      url="tbd",
      dependency_links = [],
      install_requires=['paramiko >= 2.0.2',
                        'isodate',
                        'argparse',
                        'pywinrm',
                        'requests >= 1',
                        'prettytable',
                        'python-dateutil',
                        'dnspython',
      packages=find_packages(),
      license='BSD (Simplified)',
      platforms='Posix; MacOS X;',
      classifiers=['Development Status :: 3 - Alpha',
                   'Intended Audience :: System Administrators',
                   'Operating System :: OS Independent',
                   'Topic :: System :: Systems Administration'],
      )
